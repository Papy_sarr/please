<?php

use yii\helpers\Url;

$this->pageTitle = Yii::t('UserModule.views_auth_resetPassword', 'Password reset');
?>
<div class="container" style="text-align: center;">
    
<img src="https://www.lejecos.com/photo/art/default/7515867-11592321.jpg?v=1425116017" class="animated slower bounceIn "  id="animated-img1" style="width:130px; height:80px">
<br />

<h1 class="animated slower bounceIn " style="" >
    
	<span style="width:100px; color:black; font-family: Palatino, URW Palladio L, serif; text-shadow: 2px 2px 5px white;">
    		Book Of Dreams 
  	</span>
    
</h1>


<span style="width:100px; color:black; font-style: italic; text-shadow: 2px 2px 5px white;" >
   <h5>
   <b>Etre créatif</b>, c'est penser à de nouvelles idées.    <b>Innover</b>, c'est faire des choses nouvelles.
    <h5>
<br> 
  </span>

    <br>
    <div class="row">
        <div class="panel panel-default animated fadeIn" style="max-width: 300px; margin: 0 auto 20px; text-align: left;">
            <div class="panel-heading"><?php echo Yii::t('UserModule.views_auth_resetPassword_success', '<strong>Password</strong> changed!'); ?></div>
            <div class="panel-body">
                <p><?= Yii::t('UserModule.views_auth_resetPassword_success', "Your password has been successfully changed!"); ?></p><br/>
                <a href="<?= Url::home() ?>" data-ui-loader data-pjax-prevent class="btn btn-primary"><?= Yii::t('UserModule.views_auth_resetPassword_success', 'Login') ?></a>
            </div>
        </div>
    </div>
</div>
