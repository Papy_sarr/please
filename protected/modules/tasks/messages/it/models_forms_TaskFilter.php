<?php
return array (
  'Created by me' => 'Creato da me',
  'End date' => '',
  'Filter status' => '',
  'Filter tasks' => 'Filtra attività',
  'I\'m assigned' => 'Assegnato a me',
  'I\'m responsible' => 'Sono il responsabile',
  'Overdue' => 'Scaduta',
  'Spaces' => 'Space',
  'Start date' => '',
  'Status' => 'Stato',
  'Title' => 'Titolo',
);
