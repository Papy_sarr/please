<?php

use yii\helpers\Url;
use yii\helpers\Html;
use humhub\compat\CActiveForm;

$this->pageTitle = Yii::t('UserModule.views_auth_resetPassword', 'Password reset');
?>
<div class="container" style="text-align: center;">
    
<img src="https://www.lejecos.com/photo/art/default/7515867-11592321.jpg?v=1425116017" class="animated slower bounceIn "  id="animated-img1" style="width:130px; height:80px">
<br />

<h1 class="animated slower bounceIn " style="" >
    
	<span style="width:100px; color:black; font-family: Palatino, URW Palladio L, serif; text-shadow: 2px 2px 5px white;">
    		Book Of Dreams 
  	</span>
    
</h1>


<span style="width:100px; color:black; font-style: italic; text-shadow: 2px 2px 5px white;" >
   <h5>
   <b>Etre créatif</b>, c'est penser à de nouvelles idées.    <b>Innover</b>, c'est faire des choses nouvelles.
    <h5>
<br> 
  </span>

    <div class="row">
        <div id="password-recovery-form" class="panel panel-default animated bounceIn" style="max-width: 300px; margin: 0 auto 20px; text-align: left;">
            <div class="panel-heading"><?= Yii::t('UserModule.views_auth_resetPassword', '<strong>Change</strong> your password'); ?></div>
            <div class="panel-body">
                <?php $form = CActiveForm::begin(['enableClientValidation'=>false]); ?>
                
                    <?= $form->field($model, 'newPassword')->passwordInput(['class' => 'form-control', 'id' => 'new_password', 'maxlength' => 255, 'value' => ''])?>

                    <?= $form->field($model, 'newPasswordConfirm')->passwordInput(['class' => 'form-control', 'maxlength' => 255, 'value' => ''])?>

                    <?= Html::submitButton(Yii::t('UserModule.views_auth_resetPassword', 'Change password'), ['class' => 'btn btn-primary', 'data-ui-loader' => '']); ?> 

                    <a class="btn btn-primary" data-ui-loader href="<?php echo Url::home() ?>">
                        <?= Yii::t('UserModule.views_auth_resetPassword', 'Back') ?>
                    </a>

                <?php CActiveForm::end(); ?>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">

    $(function () {
        // set cursor to email field
        $('#new_password').focus();
    })

    // Shake panel after wrong validation
<?php if ($model->hasErrors()) { ?>
        $('#password-recovery-form').removeClass('bounceIn');
        $('#password-recovery-form').addClass('shake');
        $('#app-title').removeClass('fadeIn');
<?php } ?>
</script>
